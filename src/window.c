/* gjobs
 * Copyright (C) 2004 Rodrigo Moya
 *
 * Authors:
 *	 Rodrigo Moya (rodrigo@gnome-db.org)
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkbox.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkvbbox.h>
#include "gjobs.h"

static GtkWidget *main_window = NULL, *jobs_list = NULL;

static void
on_dialog_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	switch (response_id) {
	case GTK_RESPONSE_CLOSE :
		gtk_widget_destroy (main_window);
		gtk_main_quit ();
		break;
	}
}

void
load_main_window (void)
{
	GtkWidget *table, *scroll, *box, *button;
	GtkCellRenderer *renderer;
	GtkListStore *model;

	/* create the dialog */
	main_window = gtk_dialog_new_with_buttons (_("Scheduled Jobs"), NULL,
						   GTK_DIALOG_NO_SEPARATOR,
						   GTK_STOCK_HELP, GTK_RESPONSE_HELP,
						   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
						   NULL);
	gtk_window_set_gravity (GTK_WINDOW (main_window), GDK_GRAVITY_CENTER);
	g_signal_connect (G_OBJECT (main_window), "response", G_CALLBACK (on_dialog_response), NULL);
	
	table = gtk_table_new (4, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 12);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (main_window)->vbox), table, TRUE, TRUE, 0);

	/* create the task list */
	scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroll), GTK_SHADOW_IN);
	gtk_widget_show (scroll);
	gtk_table_attach (GTK_TABLE (table), scroll, 0, 1, 0, 4,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK, 6, 6);

	model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	jobs_list = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (jobs_list),
						     0, _("When"), renderer, NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (jobs_list),
						     1, _("Command"), renderer, NULL);

	gtk_widget_show (jobs_list);
	gtk_container_add (GTK_CONTAINER (scroll), jobs_list);

	/* create the button box */
	box = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (box), GTK_BUTTONBOX_START);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (box), 6);
	gtk_widget_show (box);
	gtk_table_attach (GTK_TABLE (table), box, 1, 2, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 6);

	button = gtk_button_new_from_stock (GTK_STOCK_PROPERTIES);
	gtk_widget_set_sensitive (button, FALSE);
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 6);

	button = gtk_button_new_from_stock (GTK_STOCK_DELETE);
	gtk_widget_set_sensitive (button, FALSE);
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 6);

	/* finally, display the dialog */
	gtk_widget_show (main_window);
}
