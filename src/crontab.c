/* gjobs
 * Copyright (C) 2004 Rodrigo Moya
 *
 * Authors:
 *	 Rodrigo Moya (rodrigo@gnome-db.org)
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "crontab.h"

struct _CrontabPrivate {
};

static GObjectClass *parent_class = NULL;

static void
crontab_finalize (GObject *object)
{
	if (parent_class->finalize)
		parent_class->finalize (object);
}

static void
crontab_class_init (CrontabClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = crontab_finalize;
}

static void
crontab_init (Crontab *crontab, CrontabClass *klass)
{
}

GType
crontab_get_type (void)
{
	static GType type = 0;
                                                                                  
        if (type == 0) {
                static GTypeInfo info = {
                        sizeof (CrontabClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) crontab_class_init,
                        NULL, NULL,
                        sizeof (Crontab),
                        0,
                        (GInstanceInitFunc) crontab_init
                };
		type = g_type_register_static (G_TYPE_OBJECT, "Crontab", &info, 0);
        }

        return type;
}

static gboolean
load_crontab (Crontab *crontab)
{
	return TRUE;
}

Crontab *
crontab_new (void)
{
	gchar *program;
	Crontab *crontab;

	program = g_find_program_in_path ("crontab");
	if (!program) {
		g_warning (G_STRLOC ": crontab program not found");
		return NULL;
	}

	crontab = g_object_new (TYPE_CRONTAB, NULL);
	if (!load_crontab (crontab)) {
		g_object_unref (crontab);
		return NULL;
	}

	return crontab;
}
