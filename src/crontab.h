/* gjobs
 * Copyright (C) 2004 Rodrigo Moya
 *
 * Authors:
 *	 Rodrigo Moya (rodrigo@gnome-db.org)
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __CRONTAB_H__
#define __CRONTAB_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define TYPE_CRONTAB            (crontab_get_type())
#define CRONTAB(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_CRONTAB, Crontab))
#define CRONTAB_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, TYPE_CRONTAB, CrontabClass))
#define IS_CRONTAB(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, TYPE_CRONTAB))
#define IS_CRONTAB_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TYPE_CRONTAB))

typedef struct _CrontabPrivate CrontabPrivate;

typedef struct {
	GObject parent;
	CrontabPrivate *priv;
} Crontab;

typedef struct {
	GObjectClass parent_class;
} CrontabClass;

GType    crontab_get_type (void);
Crontab *crontab_new (void);

G_END_DECLS

#endif
